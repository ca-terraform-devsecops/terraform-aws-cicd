provider "aws" {
  region = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret
}

output "AppName" {
  value = "AppName is ${var.app_name}"
}

output "Beanstalk_Env_URL" {
  value = aws_elastic_beanstalk_environment.dev-env.cname
}

output "Test_Variable" {
  value = "Hello world I ran from terraform cloud"
}
