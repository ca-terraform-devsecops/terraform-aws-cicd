//Terraform Role
resource "aws_iam_role" "terraformrole" {
  name = "TerraformRole-${local.unique_app_name}"
  assume_role_policy = "${data.aws_iam_policy_document.terraform-policy-assume-roles.json}"
  lifecycle {
    ignore_changes = ["name"]
  }
}

data "aws_iam_policy_document" "terraform-policy-assume-roles"{
  statement {
    effect = "Allow"
    principals {
      identifiers = [
        "codepipeline.amazonaws.com",
        "codebuild.amazonaws.com",
        "codedeploy.amazonaws.com",
        "elasticbeanstalk.amazonaws.com",
        "ec2.amazonaws.com",
        "s3.amazonaws.com"
      ]
      type = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "terraform_policy_document_2" {
  statement {
    resources = [
      "${aws_s3_bucket.bucket.arn}",
      "${aws_kms_key.s3_kms.arn}",
      "${aws_elastic_beanstalk_environment.dev-env.arn}",
      "${aws_ecr_repository.helloworld_ecr.arn}",
      "${aws_codepipeline.pipeline.arn}",
      "${aws_codebuild_project.codebuild.arn}"
    ]
    actions = [
      "elasticbeanstalk:CreateApplicationVersion",
      "elasticbeanstalk:UpdateEnvironment",
      "codedeploy:GetApplication",
      "codedeploy:GetDeploymentGroup",
      "codedeploy:ListApplications",
      "codedeploy:ListDeploymentGroups",
      "elasticbeanstalk:DescribeApplications",
      "elasticbeanstalk:DescribeEnvironments",
      "autoscaling:SuspendProcesses",
      "ecr:*",
      "ec2:*",
      "elastictbeanstalk:*",
      "ecs:*",
      "ecr:*",
      "elasticloadbalancing:*",
      "cloudwatch:*",
      "autosclaing:*",
      "codepipeline:*",
      "s3:*",
      "codebuild:CreateProject",
      "codebuild:DeleteProjects",
      "codebuild:BatchGetBuilds",
      "codebuild:StartBuild",
      "cloudformation:*",
      "logs:*",
      "kms:*",
      "iam:*",
      "ecr:GetAuthorizationToken",
      "autoscaling:DescribeAutoScalingGroups"
    ]
  }
}
resource "aws_iam_role_policy" "terraform_policy" {
  policy = "${data.aws_iam_policy_document.terraform_policy_document_2.json}"
  role = "${aws_iam_role.terraformrole.id}"
}

resource "aws_iam_role_policy_attachment" "admin" {
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
  role = "${aws_iam_role.terraformrole.name}"
}

//ECR Role & Policies

data "aws_iam_policy_document" "ecr-assume-role" {
  statement {
    principals {
      identifiers = ["ec2.amazonaws.com"]
      type = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "ECR-ReadAccess" {
  name = "ECR-${local.unique_app_name}"
  assume_role_policy = "${data.aws_iam_policy_document.ecr-assume-role.json}"
  lifecycle {
    ignore_changes = ["name"]
  }
}

resource "aws_iam_instance_profile" "beanstalk_ec2" {
  name = "beanstalk-${local.unique_app_name}"
  role = "${aws_iam_role.ECR-ReadAccess.name}"
  lifecycle {
    ignore_changes = ["name"]
  }
}
resource "aws_iam_role_policy_attachment" "ecs_attachment_ecr" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role = "${aws_iam_role.ECR-ReadAccess.name}"
}
