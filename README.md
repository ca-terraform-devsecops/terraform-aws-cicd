# Terraform AWS CI/CD 

Terraform scripts for creating a CI/CD environment in AWS using AWS CodePipeline and deploying to Beanstalk 

Currently only works with a GitHub repository. 

![DevSecOps Diagram](https://devsecops.clearavenue.com/static/media/Diagram.b95a81ab.svg)

There needs to be a `terraform.tfvars` file with all the variables set. 

Rename `sample.terraform.tfvars` to `terraform.tfvars`

`aws_account_id` is your AWS Account ID. This can be found in your URL or under 'My Account' if you are a root user.

`aws_access_key` is the AWS Access Key for the generated user.

`aws_secret` is the AWS Secret for the generated user.

`git_owner` is the GitHub owner. Usually your username or organization/company name. 

`git_repo` is the name of the Repository inside of the GitHub account. 

`git_branch` is the branch that should be used for the CI/CD pipelines and deployment

`git_oauth_token` is the GitHub OAuth Token

`aws_region` is the AWS Region that should be used in the CI/CD pipelines and deployments.

`app_name` is the Application Name

`unique_name` is the **unique** name of the application. This must be a UUID ex: 844d922c-f0d9-11e9-81b4-2a2ae2dbcce4
You can use [a generator](https://www.uuidgenerator.net/) to generate a UUID

